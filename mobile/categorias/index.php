
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
  .bg-interna{
    background: #f5f5f5 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>



</head>

<body class="bg-interna">


  <?php
  require_once("../includes/topo.php") ?>

  <div class="row">

    <div class="col-12 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>CATEGORIAS</h6>
      <amp-img class="" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_paginas.png" alt="" height="3" width="88"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>





<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->
<div class="row top30">
      
    <!-- <div class="col-12"> <button class="btn btn_fechar" on="tap:my-lightbox555.close">Fechar</button></div> -->

    <!-- ======================================================================= -->
    <!-- menu lateral -->
    <!-- ======================================================================= -->
    <?php

    $result = $obj_site->select("tb_categorias_produtos","ORDER BY titulo ASC");
    if (mysql_num_rows($result) > 0) {
      $i = 0;
      while($row = mysql_fetch_array($result)){

        ?>

        <div class="col-12 menu_lateral pb20 pt20">
          <div class="list-group">

            <a class="list-group-item active" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <div class="text-center">  <h2><?php Util::imprime($row[titulo]); ?></h2></div>
            </a>

            <?php
            $result1 = $obj_site->select("tb_subcategorias_produtos", "and id_categoriaproduto = $row[0] ");
            if (mysql_num_rows($result1) > 0) {
              while($row1 = mysql_fetch_array($result1)){
                ?>
                <!-- ======================================================================= -->
                <!-- lista menu    -->
                <!-- ======================================================================= -->
                
                <div class="media linha_bottom_branca">
                  <amp-img
                  class="d-flex align-self-center mr-3"
                  height="30"
                  width="30"
                  src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row1[imagem]); ?>" alt="<?php Util::imprime($row1[titulo]); ?>" >
                </amp-img>

                <div class="media-body">
                  <a class="list-group-item  list-group-item-action " href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row1[titulo]); ?>">
                    <?php Util::imprime($row1[titulo]); ?>
                  </a>
                </div>
              </div>


              <!-- ======================================================================= -->
              <!-- lista menu    -->
              <!-- ======================================================================= -->
              <?php
            }
          }
          ?>
        </div>
      </div>

      <?php

      if ($i == 0) {
        echo '<div class="clearfix"></div>';
        $i = 0;
      }else{
        $i++;
      }

    }
  }
  ?>

  <!-- ======================================================================= -->
  <!-- menu lateral -->
  <!-- ======================================================================= -->

</div>
<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->

<?php require_once("../includes/rodape.php") ?>

</body>



</html>
