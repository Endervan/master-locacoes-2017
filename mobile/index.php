
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>

  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>

</head>



<body class="bg-index">


  <div class="row">
    <div class="col-12 text-center topo">
      <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="66" width="480"></amp-img>
    </div>
  </div>


  <div class="row font-index">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
    <div class="col-12 text-center bottom50 localizacao-pagina">
      <h3>  LOCAÇÃO DE EQUIPAMENTOS</h3>
      <div class="col-8 col-offset-2">
        <p>  PARA CONSTRUÇÃO CIVIL</p>
      </div>
    </div>



    <div class="col-4 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
        <div class="col-12 padding0 index-bg-icones">
          <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_produtos_home.png" alt="PRODUTOS" height="60" width="60"></amp-img>
          <h1>
            <amp-fit-text
						width="200"
						height="50"
            layout="responsive"
            max-font-size="17">
            PRODUTOS
          </amp-fit-text>
        </h1>
      </div>
    </a>
  </div>

  <div class="col-4 text-center">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">
      <div class="index-bg-icones">

        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_servicos_home.png" alt="SERVIÇOS" height="60" width="60"></amp-img>
        <h1>
          <amp-fit-text
					width="220"
					height="50"
          layout="responsive"
          max-font-size="17">
          SERVIÇOS
        </amp-fit-text>
      </h1>
    </div>
  </a>
</div>

<div class="col-4 text-center">
  <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
    <div class="index-bg-icones">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_orcamento_home.png" alt="ORÇAMENTO" height="60" width="60"></amp-img>
      <h1>
        <amp-fit-text
				width="200"
				height="50"
        layout="responsive"
        max-font-size="17">
        ORÇAMENTO
      </amp-fit-text>
    </h1>
  </div>
</a>
</div>


<div class="clearfix">  </div>

<div class="col-4 top10 text-center">
  <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">
    <div class="index-bg-icones">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_empresa_home.png" alt="A EMPRESA" height="60" width="60"></amp-img>
      <h1>
        <amp-fit-text
				width="200"
        height="50"
        layout="responsive"
        max-font-size="17">
        A EMPRESA
      </amp-fit-text>
    </h1>
  </div>
</a>
</div>

<div class="col-4  top10 text-center">
  <a href="<?php echo Util::caminho_projeto() ?>/mobile/contatos">
    <div class="index-bg-icones">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_contatos_home.png" alt="CONTATOS" height="60" width="60"></amp-img>
      <h1>
        <amp-fit-text
        width="200"
        height="50"
        layout="responsive"
        max-font-size="17">
        CONTATOS
      </amp-fit-text>
    </h1>
  </div>
</a>
</div>



<div class="col-4  top10 text-center">
  <a   on="tap:my-lightbox-map"
    role="a"
    tabindex="0">
    <div class="index-bg-icones">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_local.png" alt="MAPA" height="60" width="60"></amp-img>
      <h1>
        <amp-fit-text
        width="200"
        height="50"
        layout="responsive"
        max-font-size="17">
        MAPA
      </amp-fit-text>
    </h1>
  </div>
</a>
</div>


<amp-lightbox
id="my-lightbox-map"
layout="nodisplay">
<div class="lightbox"
on="tap:my-lightbox-map.close"
role="a"
tabindex="0">

<!-- ======================================================================= -->
<!-- MAPAS DAS LOJAS  -->
<!-- ======================================================================= -->
<div class="row ">

    <div class="col-12">  <button class="btn btn_fechar" on="tap:my-lightbox-map.close">Fechar</button></div>
    <?php
    $result = $obj_site->select("tb_lojas");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        $i=0;
        ?>

        <div class="col-12 top20">
          <div class="titulo_loja">

            <div class=" text-center pt5">
              <h2 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
            </div>

            <div class="clearfix"></div>
            <?php if (!empty($row[link_maps])) :?>
            <div class="col-12 text-center ">
              <a class="btn btn_telefone input100" href="<?php Util::imprime($row[link_maps]); ?>">
                ABRIR MAPA
              </a>
            </div>
            <?php endif;?>

          </div>
        </div>





        <?php
        if ($i==0) {
          echo "<div class='clearfix'>  </div>";
        }else {
          $i++;
        }
      }
    }
    ?>

</div>

<!-- ======================================================================= -->
<!-- MAPAS DAS LOJAS  -->
<!-- ======================================================================= -->



</div>
</amp-lightbox>




</div>


<?php require_once("./includes/rodape.php") ?>


</body>



</html>
