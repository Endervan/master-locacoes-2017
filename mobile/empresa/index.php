<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>



</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">

    <div class="col-12 text-center localizacao-pagina titulo_emp">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>EMPRESA</h6>
      <amp-img class="" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_paginas.png" alt="" height="3" width="88"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>


  <?php require_once("../includes/lojas.php") ?>


  <div class="row">

  <div class="col-12 padding0">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/fundo_empresa.jpg"
        width="480"
        height="250"
        layout="responsive"
        alt="AMP">
      </amp-img>
  </div>

</div>






<div class="row empresa">
  <!-- ======================================================================= -->
  <!-- conheca nossa empresa  -->
  <!-- ======================================================================= -->
  <div class="col-12 top20">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
    <div class="top10">
      <h1><?php Util::imprime($row[titulo]); ?></h1>
    </div>
    <div class="top30">
      <p><?php Util::imprime($row[descricao]); ?></p>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- conheca nossa empresa  -->
  <!-- ======================================================================= -->
</div>


<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row bottom50">
  <div class="col-11 top40">

    <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/produtos_servicos.png"
      width="350"
      height="79"
      layout="responsive"
      alt="AMP">
    </amp-img>

  </div>

  <div class="clearfix">  </div>

  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
        width="125"
        height="125"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>

  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
        width="125"
        height="125"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>


  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
        width="125"
        height="125"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>



</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
