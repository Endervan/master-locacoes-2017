<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>

</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">

    <div class="col-12 text-center localizacao-pagina titulo_emp">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>SERVIÇOS</h6>
      <amp-img class="" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_paginas.png" alt="" height="3" width="88"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>



  <?php require_once("../includes/lojas.php") ?>


  <div class="row servicos">
    <?php
    $result = $obj_site->select("tb_servicos");
    if(mysql_num_rows($result) > 0){
      $i = 0;
      while($row = mysql_fetch_array($result)){

        ?>

        <div class="col-10 col-offset-1  top20">

          <div class="relativo">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <amp-img
              layout="responsive"
              height="200"
              width="350"
              src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem_principal]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
            </amp-img>

            <div class="col-10">
              <div class="btn btn_servico"><h3><span><?php Util::imprime($row[titulo]); ?></span></h3></div>
            </div>
          </a>
        </div>



        <div class="pt50 bg_cinza pb20 pg10 "><p class=" desc_servico"><?php Util::imprime($row[descricao],300); ?></p></div>

        <div class="col-10 col-offset-1 text-right">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais_servicos">MAIS DETALHES</a>
        </div>

      </div>


      <?php
      if ($i==0) {
        echo "<div class='clearfix'>  </div>";
      }else {
        $i++;
      }
    }
  }
  ?>


</div>




<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row bottom50">
  <div class="col-11 top40">

    <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/produtos_servicos.png"
      width="350"
      height="79"
      layout="responsive"
      alt="AMP">
    </amp-img>

  </div>

  <div class="clearfix">  </div>

  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
        width="125"
        height="125"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>

  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
        width="125"
        height="125"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>


  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
        width="125"
        height="125"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>



</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
