<div class="row">
  <div class="separador-rodape clearfix"></div>
  <div class="rodape col-12">
    <div class="col-6 top5">
      <a
      on="tap:my-lightbox"
      role="a"
      tabindex="0">
      <i class="fa fa-phone" aria-hidden="true"></i> LIGAR AGORA
    </a>
  </div>
  <div class="col-6 top5 text-right">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
      MEU ORÇAMENTO <i class="fa fa-shopping-cart" aria-hidden="true"></i>
    </a>
  </div>
</div>
</div>




<div class="rodape_pai">

  <amp-lightbox scrollable
  id="my-lightbox"
  layout="nodisplay">

  
  <div class="lightbox"
  on="tap:my-lightbox.close"
  role="a"
  tabindex="0">

  <!-- ======================================================================= -->
  <!-- nossas lojas  -->
  <!-- ======================================================================= -->
  <div class="row ">
    <div class="">
      <div class="col-12">  <button class="btn btn_fechar" on="tap:my-lightbox.close">Fechar</button></div>
      <?php
      $result = $obj_site->select("tb_lojas");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          $i=0;
          ?>

          <div class="col-12 top20">
            <div class="titulo_loja">

              <div class=" text-center pt5">
                <h2 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
              </div>

              <div class="clearfix"></div>

              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              <div class="col-12 padding0 bg_branco">
                <div class=" col-12 padding0 media top10">
                  <div class="col-1 top5 padding0 media-left media-middle">
                    <amp-img class="media-object" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_telefone.png" height="20" width="20" alt=""></amp-img>
                  </div>
                  <div class="col-7 top5 media-heading media-body">
                    <h3 class="">
                      <?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>
                    </h3>

                  </div>
                  <div class="col-4">
                    <a class="btn btn_telefone input100" href="tel:+55<?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?>">
                      CHAMAR
                    </a>
                  </div>
                </div>
              </div>

              <?php if (!empty($row[telefone2])): ?>
                <div class="col-12 padding0 bg_branco">
                  <div class=" col-12 padding0 media top10">
                    <div class="col-1 top5 padding0 media-left media-middle">
                      <i class="fa fa-whatsapp fa-2x right5"></i>
                    </div>
                    <div class="col-7 top5 media-heading media-body">
                      <h3 class="">
                        <?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>
                      </h3>

                    </div>
                    <div class="col-4">
                      <a class="btn btn_telefone input100" href="tel:+55<?php Util::imprime($row[ddd2]); ?> <?php Util::imprime($row[telefone2]); ?>">
                        CHAMAR
                      </a>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->


            </div>
          </div>





          <?php
          if ($i==0) {
            echo "<div class='clearfix'>  </div>";
          }else {
            $i++;
          }
        }
      }
      ?>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- nossas lojas  -->
  <!-- ======================================================================= -->



</div>
</amp-lightbox>



</div>
