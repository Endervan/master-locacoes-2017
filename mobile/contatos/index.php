<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>


  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success > input .btn .btn_formulario {
    display: none
  }


  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


</head>

<body class="bg-interna">


  <?php  require_once("../includes/topo.php")?>

  <div class="row">

    <div class="col-12 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>CONTATOS</h6>
      <amp-img class="" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_paginas.png" alt="" height="3" width="88"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>


  <?php require_once("../includes/lojas.php") ?>




<div class="row bottom30 fundo_contatos">

  <!-- ======================================================================= -->
  <!-- FORMULARIO   -->
  <!-- ======================================================================= -->
  <div class="col-12">

    <form method="post" class="p2" action-xhr="envia.php" target="_top">

      <div class="top20">
        <h5>ENVIE SUA MENSAGEM</h5>
      </div>
      <div class="ampstart-input inline-block  form_contatos m0 p0 mb3">
        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
          <span class="fa fa-user form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
          <span class="fa fa-envelope form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
          <span class="fa fa-phone form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="assunto" placeholder="ASSUNTO" required>
          <span class="fa fa-star form-control-feedback"></span>
        </div>

        <div class="relativo">
          <input type="text" class="input-form input100 block border-none p0 m0" name="fala" placeholder="FALR COM" required>
          <span class="fa fa-users form-control-feedback"></span>
        </div>

        <div class="relativo">
          <textarea name="mensagem" placeholder="MENSAGEM" class="input-form1 input100 campo-textarea" ></textarea>
          <span class="fa fa-pencil form-control-feedback"></span>
        </div>

      </div>

      <div class="col-12 ">
        <div class="relativo text-center">

          <input type="submit" value="ENVIAR" class="ampstart-btn caps btn btn_formulario col-5">

        </div>
      </div>


      <div submit-success>
        <template type="amp-mustache">
          Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
        </template>
      </div>

      <div submit-error>
        <template type="amp-mustache">
          Houve um erro, {{name}} por favor tente novamente.
        </template>
      </div>

    </form>
  </div>
  <!-- ======================================================================= -->
  <!-- FORMULARIO   -->
  <!-- ======================================================================= -->

</div>





<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class="row top40 bottom50">
  <div class="col-8">
    <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/localizacao.png"
      width="207"
      height="50"
      layout="responsive"
      alt="">
    </amp-img>
  </div>

  <div class="col-12 top5 padding0">
    <amp-iframe
    width="480"
    height="300"
    layout="responsive"
    sandbox="allow-scripts allow-same-origin allow-popups"
    frameborder="0"
    src="<?php Util::imprime($config[src_place]); ?>">
  </amp-iframe>

</div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
