<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relativo">
    <div class="row">
      <div id="container_banner">
        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>


    <!-- ======================================================================= -->
    <!-- CATEGORIAS HOME -->
    <!-- ======================================================================= -->

    <div class="row position_cat">
      <div class="container">
        <div class="row lista_categorias">
          <div class="col-12 padding0 top30">
            <div id="slider1" class="flexslider">
              <div class="barra_cat">  </div>
              <ul class="slides">
                <?php
                $i=0;
                $result1 = $obj_site->select("tb_categorias_produtos");
                if (mysql_num_rows($result1) > 0) {

                  while ($row1 = mysql_fetch_array($result1)) {

                    ?>
                    <li class="text-center top10">
                      <a  href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row1[titulo]); ?>" >
                        <img  width="130" height="100" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row1[imagem_icon]); ?>" alt="" /> <br>
                        <div class="line_cat"><span class="pt20 "><?php Util::imprime($row1[titulo]); ?></span></div>
                      </a>
                    </li>
                    <?php
                  }

                }
                ?>

                <li class="active">
                  <a class="btn todas_cat" href="<?php echo Util::caminho_projeto() ?>/produtos" title="" >
                    VER TODOS <br> AS CATEGORIAS
                  </a>
                </li>

              </ul>
            </div>
          </div>


            <!--  ==============================================================  -->
            <!--   PESQUISAR -->
            <!--  ==============================================================  -->
            <div class="col-3 top25 ml-auto procura_empresa">
              <div class="pg5 bg_cinza">
                <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                  <div class="input-group">
                    <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                    <span class="input-group-btn">
                      <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                    </span>
                  </div>
                </form>
              </div>
            </div>
            <!--  ==============================================================  -->
            <!--   PESQUISAR -->
            <!--  ==============================================================  -->



        </div>
      </div>
    </div>

    <!-- ======================================================================= -->
    <!-- CATEGORIAS HOME -->
    <!-- ======================================================================= -->



  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->




  <!--  ==============================================================  -->
  <!--   PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container-fluid top90 relativo">
    <div class="row">
      <div class="barra_site pg10"> <h3>NOSSOS DESTAQUES</h3></div>
      <div class="linha_prod">  </div>


      <div class="container top80">
        <div class="row produtos">



          <?php
          $i = 0;
          $result = $obj_site->select("tb_produtos", "and exibir_home = 'SIM' order by rand() limit 6");
          if(mysql_num_rows($result) == 0){
            echo "<h2 class='bg-info clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
          }else{
            while ($row = mysql_fetch_array($result))
            {
              $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
              $row_categoria = mysql_fetch_array($result_categoria);
              ?>
              <div class="col-4 top30">
                <div class="card  text-center">

                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",340, 207, array("class"=>"w-100 imagem", "alt"=>"$row[titulo]")) ?>
                  </a>


                  <div class="card-body text-left">
                    <div class="line_text_prod "><h1 class="card-title  "><?php Util::imprime($row[titulo]); ?></h1></div>

                    <div class="row top10">
                      <div class="col-12 padding0 text-center">
                        <a class="btn btn_detalhes_produtos" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
                          MAIS DETALHES
                        </a>
                        <a href="javascript:void(0);" class="btn btn_produtos_orcamento left10" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                          ORÇAMENTO<img class="left5 middle" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_orcamento.png" alt="">
                        </a>
                      </div>
                    </div>

                  </div>
                </div>
              </div>

              <?php
              if($i == 2){
                echo '<div class="clearfix"></div>';
                $i = 0;
              }else{
                $i++;
              }

            }
          }
          ?>

          <div class="col-12 text-right top80">
            <a class="btn btn_detalhes_produtos_todos" href="<?php echo Util::caminho_projeto() ?>/produtos"  title="TODOS PRODUTOS">
              VER TODOS PRODUTOS
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   PRODUTOS -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--   SERVICOS -->
  <!--  ==============================================================  -->
  <div class="container-fluid bg_servicos_home top50 pt40 pb80 relativo">
    <div class="row">
      <div class="barra_site pg10"> <h3>NOSSOS SERVIÇOS</h3></div>
      <div class="linha_prod_servico">  </div>


      <div class="container bottom75">
        <div class="row servicos">

          <?php
          $result = $obj_site->select("tb_servicos","order by rand() limit 2");
          if(mysql_num_rows($result) > 0){
            $i=0;
            while($row = mysql_fetch_array($result)){
              if ($i == 0) {
                $ml_auto = 'mr-auto';
                $offset="col-offset-5";
                $i++;
              }else{
                $ml_auto = 'mr-auto';
                $offset="col-offset-6";
                $i = 0;
              }

              ?>

              <div class="row">
                <div class="col-5 <?php echo $ml_auto; ?> <?php echo $offset; ?> top60">
                  <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
                    <div class="media">
                      <img width="60" height="60" class="d-flex align-self-center mr-3" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="">

                      <div class="media-body">
                        <div class="line_servico">  <h2 class="mt-0"><?php Util::imprime($row[titulo]); ?></h2></div>
                        <div class="line_servico_des">  <p><?php Util::imprime($row[descricao],500); ?></p></div>
                      </div>
                    </div>
                  </a>

                  <a class="btn btn_detalhes_servicos pull-right top10" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
                    CONHEÇA MAIS
                  </a>
                </div>

              </div>
              <?php
              if ($i==0) {
                echo "<div class='clearfix'>  </div>";
              }else {
                $i++;
              }
            }
          }
          ?>


        </div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   SERVICOS -->
  <!--  ==============================================================  -->





  <!--  ==============================================================  -->
  <!--   EMPRESA HOME -->
  <!--  ==============================================================  -->
  <div class="container-fluid top100 bottom140 relativo">
    <div class="row">

      <div class="bg_empresa_home">  </div>

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>

      <div class="barra_site pg10"> <h3><?php Util::imprime($row[titulo]); ?></h3></div>


      <div class="container">
        <div class="row top100">
          <div class="col-7">

            <div class="">
              <p><span><?php Util::imprime($row[descricao]); ?></span></p>
            </div>

            <a class="btn btn_detalhes_servicos pull-right top10" href="<?php echo Util::caminho_projeto() ?>/empresa"  title="<?php Util::imprime($row[titulo]); ?>">
              SAIBA MAIS
            </a>

          </div>
        </div>

      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA HOME -->
  <!--  ==============================================================  -->
<?php /*


*/ ?>

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true,
      delay:6000
    }

  });
});
</script>





<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: false,/*tira bolinhas*/
    animationLoop: true,
    itemWidth: 195,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
