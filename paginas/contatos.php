<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background: #fefefe url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 108px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top90 bottom50">
        <h3>CONTATOS</h3>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <div class="container-fluid bg_verde">
    <div class="row">
      <div class="barra_site top100 pg10"> <h3>FALE CONOSCO</h3></div>
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <ol class="breadcrumb endereco_sites top20">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5 active">CONTATOS</li>
            </ol>
          </div>
        </div>

        <div class="row">
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->
          <div class="col-3 ml-auto procura_empresa">
            <div class="pg5 bg_cinza">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->

          <div class="col-3 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-lg btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>


      </div>
    </div>
  </div>

  <div class="container-fluid relativo">
    <div class="row">
      <div class="bg_contatos">  </div>
      <div class="container top65">
        <div class="row ">

          <div class="col-4">





            <!-- ======================================================================= -->
            <!-- links  -->
            <!-- ======================================================================= -->
            <ul class="nav nav-pills text-center flex-column top20">
              <li class="nav-item  col-12 top20">
                <li class="nav-item  col-12 top20">
                  <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">
                    TRABALHE CONOSCO
                  </a>
                </li>


                <li class="nav-item  col-12 top20">
                  <a class="nav-link" href="#"  onclick="$('html,body').animate({scrollTop: $('.mapa').offset().top}, 1000);">
                    NOSSA LOCALIZAÇÃO
                  </a>
                </li>


              </li>


            </ul>
            <!-- ======================================================================= -->
            <!-- links  -->
            <!-- ======================================================================= -->


            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->
            <div class="top30">
              <h2><img class="mr-1" src="./imgs/icon_telefone_contatos.png" alt="">LIGUE AGORA</h2>


              <?php
              $result = $obj_site->select("tb_lojas","limit 2");
              if(mysql_num_rows($result) > 0){
                $i=0;
                while($row = mysql_fetch_array($result)){
                  if ($i == 0) {
                    $padding = 'pl5';
                    $local='GOIÂNIA-GO';
                    $i++;
                  }else{
                    $padding = 'pl25';
                    $local='BRASÍLIA-DF';
                    $i = 0;
                  }

                  ?>
                  <div class="mt-2 ml-4">
                    <h4 class="top5 "><?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?> <span><?php echo $local; ?></span></h4>
                  </div>

                  <?php
                  if ($i==0) {
                    echo "<div class='clearfix'>  </div>";
                  }else {
                    $i++;
                  }
                }
              }
              ?>
            </div>
            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->


          </div>

          <!--  ==============================================================  -->
          <!-- FORMULARIO CONTATOS-->
          <!--  ==============================================================  -->
          <div class="col-7 top100 ml-auto fundo_formulario">
            <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">

              <div class="form-row">
                <div class="col">
                  <div class="form-group icon_form">
                    <input type="text" name="nome" class="form-control fundo-form form-control-lg input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="email" class="form-control fundo-form form-control-lg input-lg" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback"></span>
                  </div>
                </div>
              </div>

              <div class="form-row">
                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="telefone" class="form-control fundo-form form-control-lg input-lg" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="assunto" class="form-control fundo-form form-control-lg input-lg" placeholder="ASSUNTO">
                    <span class="fa fa-star form-control-feedback"></span>
                  </div>
                </div>
              </div>


              <div class="form-row">
                <div class="col">
                  <div class="form-group icon_form">
                    <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="MENSAGEM"></textarea>
                    <span class="fa fa-pencil form-control-feedback"></span>
                  </div>
                </div>
              </div>


              <div class="col-12 text-right padding0 ">
                <button type="submit" class="btn btn_formulario" name="btn_contato">
                  ENVIAR MENSAGEM
                </button>
              </div>

            </form>
          </div>

        </div>

        <!--  ==============================================================  -->
        <!-- FORMULARIO CONTATOS-->
        <!--  ==============================================================  -->

      </div>
    </div>
  </div>



  <div class="container-fluid relativo">
    <div class="row">
      <div class="barra_site_paginas top70 pt5"> <h3><img class="mr-3 "src="<?php echo Util::caminho_projeto() ?>/imgs/localizacao.png" alt="">NOSSA LOCALIZAÇÃO</h3></div>
      <div class="container  top35">
        <div class="row mapa">
          <div class="col-12  top100">
            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="443" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
  echo $texto_mensagem = "
  Nome: ".($_POST[nome])." <br />
  Email: ".($_POST[email])." <br />
  Telefone: ".($_POST[telefone])." <br />
  Assunto: ".($_POST[assunto])." <br />

  Mensagem: <br />
  ".(nl2br($_POST[mensagem]))."
  ";

  ;



  if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])) {
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
    Util::alert_bootstrap("Obrigado por entrar em contato.");
    unset($_POST);
  }else{
    Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
  }

}



?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
