<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",5) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 108px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top90 bottom50">
        <h3>PRODUTOS</h3>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <div class="container-fluid bottom150 bg_verde">
    <div class="row">
      <div class="barra_site_paginas top90 pg10"> <h3>DETALHES DO PRODUTO</h3></div>
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <ol class="breadcrumb endereco_sites top20">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>" data-toggle="tooltip" data-placement="top" title="Volta para Produtos" ><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5 active">PRODUTO</li>
            </ol>
          </div>
        </div>

        <div class="row">
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->
          <div class="col-3 ml-auto procura_empresa">
            <div class="pg5 bg_cinza">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->

          <div class="col-3 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-lg btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>


      </div>
    </div>
  </div>


  <!--  ==============================================================  -->
  <!--  DESCRICAO -->
  <!--  ==============================================================  -->
  <div class="container top60 bottom60">
    <div class="row produto_dentro">

      <div class="col-6">
        <div class="">
          <h3 class=" "><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h3>
        </div>

        <div class="top10">
          <h1><span><?php  Util::imprime( Util::troca_value_nome($dados_dentro[idsubcategoriaproduto], "tb_subcategorias_produtos", "idsubcategoriaproduto", "titulo")); ?></span></h1>
        </div>


        <div class="desc_geral_produto top20">
          <p class="w-100"><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>

        <div class="top30">
          <a href="javascript:void(0);" class="btn btn_produtos_orcamento" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
            ADICIONAR AO ORÇAMENTO
          </a>
        </div>

      </div>



    </table>
    <div class="col-6">

      <!-- ======================================================================= -->
      <!-- SLIDER CATEGORIA -->
      <!-- ======================================================================= -->
      <div class="slider_prod_geral">

        <?php
        $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
        if(mysql_num_rows($result) == 0){
          ?>
          <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $dados_dentro[imagem]; ?>" class="group4">
            <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 566, 406, array("class"=>"", "alt"=>"$row[titulo]")) ?>
            <!-- <img src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" class="input100" /> -->
          </a>

          <?php
        }else{
          ?>

          <div class="zomm">  </div>

          <!-- Place somewhere in the <body> of your page -->
          <div id="slider" class="flexslider">



            <div class="row">

            </div>
            <!-- <span>Clique na imagem para Ampliar</span> -->
            <ul class="slides slider-prod">

              <?php

              {
                while($row = mysql_fetch_array($result)){
                  ?>
                  <li class="zoom">
                    <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" title="<?php echo $row[legenda]; ?>"class="group4">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 566, 406, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                      <!-- <img src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" class="input100" /> -->
                    </a>
                  </li>
                  <?php
                }
              }
              ?>
              <!-- items mirrored twice, total of 12 -->
            </ul>

          </div>

          <img class="w-100" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_produto.png" alt="">


          <!-- <div id="carousel" class="flexslider">
          <ul class="slides slider-prod-tumb"> -->
          <?php /*
          $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
          if(mysql_num_rows($result) > 0)
          {
          while($row = mysql_fetch_array($result)){
          ?>
          <li>
          <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" />
          </li>
          <?php
        }
      }

      */  ?>
      <!-- items mirrored twice, total of 12 -->
    </ul>

    <?php
  }
  ?>
</div>



<div class="telefones_prod">
  <div class="col-8 col-offset-3 media pt50 pb40">
    <img class="d-flex align-self-start" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_prod.png" alt="">
    <div class="media-body">
      <h2>LIGUE AGORA</h2>


      <?php
      $result = $obj_site->select("tb_lojas","limit 2");
      if(mysql_num_rows($result) > 0){
        $i=0;
        while($row = mysql_fetch_array($result)){
          if ($i == 0) {
            $padding = 'pl5';
            $local='-GO';
            $i++;
          }else{
            $padding = 'pl25';
            $local='-DF';
            $i = 0;
          }

          ?>
          <div class="<?php echo  $padding ;?>">
            <h4 class="top5 "><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
              <?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?> <?php echo $local; ?>
            </h4>
          </div>

          <?php
          if ($i==0) {
            echo "<div class='clearfix'>  </div>";
          }else {
            $i++;
          }
        }
      }
      ?>
    </div>
  </div>
</div>


</div>

</div>
</div>

<!--  ==============================================================  -->
<!--   DESCRICAO -->
<!--  ==============================================================  -->





<!--  ==============================================================  -->
<!--   PRODUTOS -->
<!--  ==============================================================  -->
<div class="container-fluid top90 bottom100 relativo">
  <div class="row">
    <div class="barra_site_paginas pg10"> <h3>CONFIRA NOSSOS PRODUTOS</h3></div>


    <div class="container top80">
      <div class="row produtos">
        <?php
        $i = 0;
        $result = $obj_site->select("tb_produtos", "order by rand() limit 3");
        if(mysql_num_rows($result) == 0){
          echo "<h2 class='bg-info clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
        }else{
          while ($row = mysql_fetch_array($result))
          {
            $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
            $row_categoria = mysql_fetch_array($result_categoria);
            ?>
            <div class="col-4 top30">
              <div class="card  text-center">

                <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",340, 207, array("class"=>"w-100 imagem", "alt"=>"$row[titulo]")) ?>
                </a>


                <div class="card-body text-left">
                  <div class="line_text_prod "><h1 class="card-title  "><?php Util::imprime($row[titulo]); ?></h1></div>

                  <div class="row top10">
                    <div class="col-12">
                      <a class="btn btn_detalhes_produtos" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
                        MAIS DETALHES
                      </a>
                      <a href="javascript:void(0);" class="btn btn_produtos_orcamento left10" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                        ORÇAMENTO<img class="left5 middle" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_orcamento.png" alt="">
                      </a>
                    </div>
                  </div>

                </div>
              </div>
            </div>

            <?php
            if($i == 2){
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }

          }
        }
        ?>


      </div>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!--   PRODUTOS -->
<!--  ==============================================================  -->

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<?php require_once('./includes/js_css.php') ?>




<script type="text/javascript">
$(window).load(function() {
  $('#slider').flexslider({
    animation: "slide",
    controlNav: true,
    animationLoop: true,
    slideshow: false,
  });
});
</script>
