<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: #fefefe url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 108px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      <div class="col-6 top90 bottom50">
        <h3>A EMPRESA</h3>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <div class="container-fluid bg_verde">
    <div class="row">


      <div class="barra_site top100 pg10"> <h3>QUEM SOMOS</h3></div> -->

      <div class="container">
        <div class="row ">
          <div class="col-6">
            <ol class="breadcrumb endereco_sites top20">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5 active">A EMPRESA</li>
            </ol>
          </div>

        </div>

        <div class="row">


          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->
          <div class="col-3 ml-auto procura_empresa">
            <div class="pg5 bg_cinza">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->

          <div class="col-3 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-lg btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>


      </div>
    </div>
  </div>




  <!-- ======================================================================= -->
  <!--  DESCRICAO EMPRESA -->
  <!-- ======================================================================= -->

  <div class="container-fluid fundo_empresa  top130">
    <div class="row">

      <div class="container">
        <div class="row ">
          <?php  $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
          <div class="col-6 mr-auto top90">
            <h2><span><?php Util::imprime($row[titulo]) ?></span></h2>
          </div>

          <div class="col-10 top60">
            <p><?php  Util::imprime($row[descricao]) ?></p>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  DESCRICAO EMPRESA -->
  <!-- ======================================================================= -->


<div class="container-fluid">
  <div class="row">
    <!-- ======================================================================= -->
    <!-- mapa   -->
    <!-- ======================================================================= -->
    <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="443" frameborder="0" style="border:0" allowfullscreen></iframe>
    <!-- ======================================================================= -->
    <!-- mapa   -->
    <!-- ======================================================================= -->

  </div>
</div>



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
