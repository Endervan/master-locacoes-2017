<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: #fefefe url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 108px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top90 bottom50">
        <h3>SERVIÇOS</h3>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <div class="container-fluid bottom150 bg_verde">
    <div class="row">
      <div class="barra_site_paginas top100 pg10"> <h3>CONHEÇA MAIS OS SERVIÇOS</h3></div>
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <ol class="breadcrumb endereco_sites top20">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>" data-toggle="tooltip" data-placement="top" title="Volta para Serviços" ><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5 active">SERVIÇOS</li>
            </ol>
          </div>
        </div>

        <div class="row">
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->
          <div class="col-3 ml-auto procura_empresa">
            <div class="pg5 bg_cinza">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->

          <div class="col-3 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-lg btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>


      </div>
    </div>
  </div>



  <!-- ======================================================================= -->
  <!-- SERVICOS    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row servicos_geral">

      <?php

      $result = $obj_site->select("tb_servicos");
        $i=0;
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){


          ?>
          <div class="col-4 text-center top30">
            <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

              <div class="card">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem_principal]",340, 200, array("class"=>"w-100 card-img-top", "alt"=>"$row[titulo]")) ?>
                <div class="card-body">
                  <div class="col-11 line_servico btn">  <h2 class="mt-0 card-title"><?php Util::imprime($row[titulo]); ?></h2></div>
                  <div class="line_servico_des top35">  <p><?php Util::imprime($row[descricao]); ?></p></div>
                </a>

                <a class="btn btn_detalhes_servicos_geral pull-right top10" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
                  VEJA MAIS
                </a>
              </div>
            </div>
          </div>

          <?php
          if ($i==2) {
            echo "<div class='clearfix'>  </div>";
          }else {
            $i++;
          }
        }
      }
      ?>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SERVICOS    -->
  <!-- ======================================================================= -->



    <!--  ==============================================================  -->
    <!--   PRODUTOS -->
    <!--  ==============================================================  -->
    <div class="container-fluid top90 bottom100 relativo">
      <div class="row">
        <div class="barra_site_paginas pg10"> <h3>CONFIRA NOSSOS PRODUTOS</h3></div>


        <div class="container top80">
          <div class="row produtos">
            <?php
            $i = 0;
            $result = $obj_site->select("tb_produtos", "order by rand() limit 3");
            if(mysql_num_rows($result) == 0){
              echo "<h2 class='bg-info clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
            }else{
              while ($row = mysql_fetch_array($result))
              {
                $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
                $row_categoria = mysql_fetch_array($result_categoria);
                ?>
                <div class="col-4 top30">
                  <div class="card  text-center">

                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",340, 207, array("class"=>"w-100 imagem", "alt"=>"$row[titulo]")) ?>
                    </a>


                    <div class="card-body text-left">
                      <div class="line_text_prod "><h1 class="card-title  "><?php Util::imprime($row[titulo]); ?></h1></div>

                      <div class="row top10">
                        <div class="col-12">
                          <a class="btn btn_detalhes_produtos" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
                            MAIS DETALHES
                          </a>
                          <a href="javascript:void(0);" class="btn btn_produtos_orcamento left10" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                            ORÇAMENTO<img class="left5 middle" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_orcamento.png" alt="">
                          </a>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <?php
                if($i == 2){
                  echo '<div class="clearfix"></div>';
                  $i = 0;
                }else{
                  $i++;
                }

              }
            }
            ?>


          </div>
        </div>
      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   PRODUTOS -->
    <!--  ==============================================================  -->




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
