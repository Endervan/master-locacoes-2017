<?php

$url1 = Url::getURL(1);
$url2 = Url::getURL(2);

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: #f5f5f5 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 108px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'produtos';// link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row titulo_produto">
      <div class="col-12 top30 bottom50">
        <?php
        if (isset( $url2 )) {
          $id_categoria = $obj_site->get_id_url_amigavel("tb_subcategorias_produtos", "idsubcategoriaproduto", $url2);
          $complemento_cat .= "AND idsubcategoriaproduto = '$id_categoria' ";

          $result = $obj_site->select("tb_subcategorias_produtos", $complemento_cat);
          $dados_dentro_cat = mysql_fetch_array($result);

        }


        ?>
        <h3>EQUIPAMENTO(S)  <br> PARA  <span class=" "><?php Util::imprime( Util::troca_value_nome($dados_dentro_cat[idsubcategoriaproduto], "tb_subcategorias_produtos", "idsubcategoriaproduto", "titulo")); ?></span></h3>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->

  <div class="container-fluid bg_verde">
    <div class="row">
      <div class="barra_site top100 pg10"> <h3>NOSSOS PRODUTOS</h3></div>
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <ol class="breadcrumb endereco_sites top20">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>" data-toggle="tooltip" data-placement="top" title="Volta para Produtos" ><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5 active">EQUIPAMENTOS</li>
            </ol>
          </div>
        </div>

        <div class="row">
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->
          <div class="col-3 ml-auto procura_empresa">
            <div class="pg5 bg_cinza">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->

          <div class="col-3 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/equipamentos" class="btn btn-lg btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>


      </div>
    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->
  <div class="container pb80">
    <div class="row">


      <div class="col-12 top15 padding0">
        <div class="row produtos">

          <?php



          //  FILTRA AS CATEGORIAS
          if (isset( $url1 )) {
            $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
            $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
          }


          //  FILTRA AS SUBCATEGORIAS
          if (isset( $url2 )) {
            $id_subcategoria = $obj_site->get_id_url_amigavel("tb_subcategorias_produtos", "idsubcategoriaproduto", $url2);
            $complemento .= "AND id_subcategoriaproduto = '$id_subcategoria' ";
          }



           //FILTRA PELO TITULO
          if(isset($_POST[id_categoriaproduto]) and !empty($_POST[id_categoriaproduto])):
            $complemento .= "AND id_categoriaproduto = '$_POST[id_categoriaproduto]' ";
          endif;

          //  FILTRA PELO TITULO
          if(isset($_POST[id_subcategoriaproduto]) and !empty($_POST[id_subcategoriaproduto]) ):
            $complemento .= "AND id_subcategoriaproduto = '$_POST[id_subcategoriaproduto]' ";
          endif;

          //  FILTRA PELO TITULO
          if(isset($_POST[busca_produtos])):
            $complemento .= "AND titulo LIKE '%$_POST[busca_produtos]%'";
          endif;

          ?>

          <?php

          //  busca os produtos sem filtro
          $result = $obj_site->select("tb_produtos", $complemento);

          if(mysql_num_rows($result) == 0){
            echo "
            <div class='col-12 text-center '>
            <h1 class='btn_nao_encontrado' style='padding: 100px;'>Nenhum produto encontrado.</h1>
            </div>"
            ;
          }else{
            ?>
            <div class="col-12 bottom20 top100 total-resultado-busca">
              <h2><span class="font-weight-bold"><?php echo mysql_num_rows($result) ?></span> EQUIPAMENTO(S) ENCONTRADO(S) .</h2>
            </div>

            <?php
            require_once('./includes/lista_equipamentos.php');

          }
          ?>

        </div>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
