
<div class="container-fluid bg_topo">
  <div class="row">

    <div class="container ">
      <div class="row">
        <?php
        if(empty($voltar_para)){
          $link_topo = Util::caminho_projeto()."/";
        }else{
          $link_topo = Util::caminho_projeto()."/".$voltar_para;
        }
        ?>

        <div class="col-3 top5 logo">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
          </a>
        </div>

        <div class="col-9">
          <div class=" top10">
            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->
            <div class="telefone_topo">
              <div class="row">


                <?php


                $result = $obj_site->select("tb_lojas","limit 2");
                if(mysql_num_rows($result) > 0){
                  $i=0;
                  while($row = mysql_fetch_array($result)){


                    if ($i == 0) {
                      $ml_auto = 'ml-auto';
                      $local='-GO';
                      $i++;
                    }else{
                      $ml_auto = '';
                      $local='-DF';
                      $i = 0;
                    }

                    ?>

                    <div class="col-4 padding0 media <?php echo $ml_auto; ?>">
                      <h4 class="mr-3"><span><?php Util::imprime($row[titulo]); ?> <?php echo $local; ?></span></h4>
                      <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
                      <div class="media-body align-self-center">
                        <h4 class="mt-0 ml-3"><?php Util::imprime($row[ddd1]); ?> <?php Util::imprime($row[telefone1]); ?></h4>
                      </div>
                    </div>
                    <?php
                    if ($i==0) {
                      echo "<div class='clearfix'>  </div>";
                    }else {
                      $i++;
                    }
                  }
                }
                ?>


              </div>
            </div>

            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->



            <div class="clearfix">  </div>
            <!--  ==============================================================  -->
            <!-- MENU-->
            <!--  ==============================================================  -->
            <div class="col-12 top20 text-center menu">
              <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
              <div id="navbar">
                <ul class="nav navbar-nav navbar-right align-self-center">
                  <li class="col">
                    <a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/">HOME
                    </a>
                  </li class="col">

                  <li>
                    <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
                    </a>
                  </li>



                  <li class="col">
                    <a class="<?php if(Url::getURL( 0 ) == "orcamento" or Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "equipamentos"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
                    </a>
                  </li>


                  <li class="col">
                    <a class="<?php if(Url::getURL( 0 ) == "orcamento" or Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
                    </a>
                  </li>


                  <li class="col">
                    <a class="<?php if(Url::getURL( 0 ) == "contatos" or Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS
                    </a>
                  </li>





                  <!--  ==============================================================  -->
                  <!--CARRINHO-->
                  <!--  ==============================================================  -->
                  <div class="padding0  carrinho">

                    <div class="dropdown show">
                      <a class="btn btn_topo" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_orcamento.png" alt="" />
                        <!-- <span class="badge badge-pill"><?php //echo count($_SESSION[solicitacoes_produtos])/*+count($_SESSION[solicitacoes_servicos])*/; ?></span> -->
                      </a>

                      <div class="dropdown-menu topo-meu-orcamento" aria-labelledby="dropdownMenuLink">

                        <?php if (count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]) == 0):?>


                          <div class="col-12 text-center">
                            <h1>Nenhum Item Adicionado</h1>
                          </div>


                        <?php else:


                          ?>


                          <!--  ==============================================================  -->
                          <!--sessao adicionar produtos-->
                          <!--  ==============================================================  -->
                          <?php
                          if(count($_SESSION[solicitacoes_produtos]) > 0){
                            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                              $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                              ?>
                              <tr class="middle">
                                <div class="row lista-itens-carrinho">
                                  <div class="col-2">
                                    <?php if(!empty($row[imagem])): ?>
                                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                    <?php endif; ?>
                                  </div>
                                  <div class="col-8">
                                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                                  </div>
                                  <div class="col-1">
                                    <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                  </div>
                                  <div class="col-12"><div class="borda_carrinho top5">  </div> </div>
                                </div>
                              </tr>
                              <?php
                            }
                          }
                          ?>

                          <!--  ==============================================================  -->
                          <!--sessao adicionar produtos-->
                          <!--  ==============================================================  -->





                          <!--  ==============================================================  -->
                          <!--sessao adicionar servicos-->
                          <!--  ==============================================================  -->
                          <?php
                          if(count($_SESSION[solicitacoes_servicos]) > 0)
                          {
                            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                            {
                              $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                              ?>
                              <div class="row lista-itens-carrinho">
                                <div class="col-2">
                                  <?php if(!empty($row[imagem])): ?>
                                    <!-- <img class="carrinho_servcos" src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" alt="" /> -->
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem_principal]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                  <?php endif; ?>
                                </div>
                                <div class="col-8">
                                  <h1><?php Util::imprime($row[titulo]) ?></h1>
                                </div>
                                <div class="col-1">
                                  <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                </div>
                                <div class="col-12"><div class="borda_carrinho top5">  </div> </div>

                              </div>
                              <?php
                            }
                          }
                          ?>

                          <!--  ==============================================================  -->
                          <!--sessao adicionar servicos-->
                          <!--  ==============================================================  -->




                          <div class="col-12 text-right top10 bottom20">
                            <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn_orcamento" >
                              ENVIAR ORÇAMENTO
                            </a>
                          </div>

                        <?php endif ;?>
                      </div>

                    </div>




                  </div>
                  <!--  ==============================================================  -->
                  <!--CARRINHO-->
                  <!--  ==============================================================  -->



                </ul>
              </div><!--/.nav-collapse -->

            </div>
            <!--  ==============================================================  -->
            <!-- MENU-->
            <!--  ==============================================================  -->

          </div>



        </div>

      </div>
    </div>


  </div>
</div>
