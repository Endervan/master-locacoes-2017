
<div class="container-fluid rodape">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>


		<div class="container bottom20">
			<div class="row ">


				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-2 top10">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->



				<div class="col-10 menu_rodape ">
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<ul class="nav nav-right">
						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "equipamentos"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
							</a>
						</li>


						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "contatos" or Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS
							</a>
						</li>


						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/orcamento">ORÇAMENTO
							</a>
						</li>

					</ul>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
				</div>



<div class="clearfix"></div>




				<?php
				$result = $obj_site->select("tb_lojas","limit 2");
				if(mysql_num_rows($result) > 0){
					$i=0;
					while($row = mysql_fetch_array($result)){

						?>

						<!-- ======================================================================= -->
						<!-- unidades  -->
						<!-- ======================================================================= -->


						<div class="col-5 top15 endereco_rodape">
							<div class="row">
								<div class="col top5">
									<h5>UNIDADE <?php Util::imprime($row[titulo]); ?></h5>
									<div class="media top15">
										<img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_endereco_rodape.png" alt="">
										<div class="media-body align-self-center">
											<p class="mt-0"><?php Util::imprime($row[endereco]); ?></p>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col top10 media">
									<img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_rodape.png" alt="">
									<div class="media-body ">
										<h2 class="mt-0"><?php Util::imprime($row[ddd1]); ?>  <?php Util::imprime($row[telefone1]); ?> </h2>
									</div>
								</div>

								<?php if (!empty($row[telefone2])): ?>
								<div class="col top10 media">
									<img width="25" height="25" class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_rodape.png" alt="">
									<div class="media-body ">
										<h2 class="mt-0 mr-3"><?php Util::imprime($row[ddd2]); ?>  <?php Util::imprime($row[telefone2]); ?> </h2>
									</div>
								</div>
							<?php endif; ?>
							</div>

						</div>
						<!-- ======================================================================= -->
						<!-- unidades  -->
						<!-- ======================================================================= -->

						<?php
						if ($i==0) {
							echo "<div class='clearfix'>  </div>";
						}else {
							$i++;
						}
					}
				}
				?>




				<!-- ======================================================================= -->
				<!--homeweb   -->
				<!-- ======================================================================= -->
				<div class="col-2 ml-auto redes top40">
					<div class="row">


						<!-- ======================================================================= -->
						<!-- redes sociais    -->
						<!-- ======================================================================= -->
						<div class="col padding0 text-right top20 redes">
							<div class="">

								<?php if ($config[google_plus] != "") { ?>
									<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
										<i class="fa fa-google-plus-official fa-2x top20"></i>
									</a>
								<?php } ?>


								<?php if ($config[facebook] != "") { ?>
									<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
										<i class="fa fa-facebook-official fa-2x top20"></i>
									</a>
								<?php } ?>

								<?php if ($config[instagram] != "") { ?>
									<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
										<i class="fa fa-instagram fa-2x top20"></i>
									</a>
								<?php } ?>


							</div>
						</div>
						<!-- ======================================================================= -->
						<!-- redes sociais    -->
						<!-- ======================================================================= -->

						<div class="col">
							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
							</a>
						</div>



					</div>
				</div>
				<!-- ======================================================================= -->
				<!--homeweb   -->
				<!-- ======================================================================= -->

<?php /*
*/ ?>

			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape-preto lato">
		<div class="col-12 text-center top10 bottom10">
			<h5>© Copyright  MASTER LOCAÇÕES</h5>
		</div>
	</div>
</div>
